# Инструкция

## Загрузка

1. Используем [торрент](https://codeberg.org/CyberRedGuards/awesome-commie/src/branch/main/ru.md#%D0%B7%D0%B0%D0%B3%D1%80%D1%83%D0%B7%D0%BA%D0%B0-%D1%82%D0%BE%D1%80%D1%80%D0%B5%D0%BD%D1%82%D0%BE%D0%B2) для загрузки:

`magnet:?xt=urn:btih:BC34C8AD0C5FF3E96F780D0965F3032F5A6828EB`

2. Записываем USB образ, через [Rufus](https://codeberg.org/CyberRedGuards/awesome-commie/src/branch/main/ru.md#windows-10-%D1%83%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0-%D0%B8-%D0%BF%D1%80%D0%B0%D0%B2%D0%B8%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F-%D0%BD%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0).

3. Устанавливаем систему.

4. Запуск от имени администратора `activat.bat`.

5. Пользуемся ;)